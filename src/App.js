import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import Calendar from './components/Calendar';
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>

          <Route path='/' exact>
            <Redirect to='/calendar' />
          </Route>

          <Route path='/calendar' component={Calendar} />
          
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
