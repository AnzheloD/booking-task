import React, { useState } from 'react';
import data from '../data/calendar.json';
import { Modal } from 'react-bootstrap';
import CreateRecord from './CreateRecord.js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

const Calendar = () => {
  const [calendarData, setCalendarData] = useState(data);
  const [createMode, setModeCreate] = useState(false);

  const toggleCreateMode = () => {
    setModeCreate((prevState) => !prevState);
  };

  let recordId = 1;
  const incrementId = () => {
    return recordId++;
  }

  const loadedData = calendarData.map((record) => {
    record.id = recordId;
    incrementId();

    return (
      <tr key={record.id}>
        <th scope="row">{record.id}</th>
        <td>{record.date}</td>
        <td>{record.status}</td>
        <td>{record.price}$</td>
        {
          record.name ?
            <td>{record.name}</td>
            :
            record.status === 'blocked' ?
              <td>Not Available!!</td>
              :
              <td>Free</td>
        }
      </tr>
    )
  });

  return (
    <>
      <div className="container">
        <div className="row">

          <div className="col-12">

            <button
              type="button"
              className="btn btn-secondary mt-3 mb-3"
              onClick={toggleCreateMode}
            >
              <FontAwesomeIcon
                icon={faPlus}
                size='3x'
              />
            </button>
          </div>

          {
            createMode ?

              <Modal show={createMode} onHide={toggleCreateMode}>
                <CreateRecord
                  calendarData={calendarData}
                  toggleMode={toggleCreateMode}
                />
              </Modal>
              :
              null
          }

          <table className="table table-striped col-12">

            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Date</th>
                <th scope="col">Status</th>
                <th scope="col">Price</th>
                <th scope="col">Name</th>
              </tr>
            </thead>

            <tbody>
              {loadedData}
            </tbody>

          </table>
        </div>
      </div>
    </>
  );
}

export default Calendar;