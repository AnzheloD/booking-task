import React, { useState } from 'react';
import Swal from 'sweetalert2';
import moment from 'moment';

const CreateRecord = ({ toggleMode, calendarData }) => {
  const [isFormValid, setIsFormValid] = useState(false);
  const currentDate = moment().format('YYYY-MM-DD');

  const [form, setForm] = useState({
    name: {
      placeholder: 'Name...',
      value: '',
      validations: {
        required: true,
        minLength: 3,
        maxLength: 25
      },
      valid: false,
      touched: false,
    },
    startDate: {
      placeholder: 'Start Date...',
      value: '',
      validations: {
        required: true,
      },
      valid: false,
      touched: false
    },
    endDate: {
      placeholder: 'End Date...',
      value: '',
      validations: {
        required: true,
      },
      valid: false,
      touched: false
    }
  });

  const handleSubmit = event => {
    event.preventDefault();

    const data = Object.keys(form)
      .reduce((acc, elementKey) => {
        return {
          ...acc,
          [elementKey]: form[elementKey].value
        }
      }, {});


    const reservationDates = calendarData?.filter(record => record?.date >= data?.startDate && record?.date <= data?.endDate);
    const bookedOrBlocked = reservationDates.some(record => record?.status !== 'bookable');

    if (data.startDate < currentDate || data.endDate < currentDate || data.startDate > data.endDate) {
      Swal.fire({
        icon: 'error',
        title: 'Incorrect date!',
      });
    } else if (bookedOrBlocked) {

      Swal.fire({
        icon: 'error',
        title: 'The room is not available for these dates!',
      });
    } else {

      reservationDates?.map(record => {
        record.status = 'booked';
        record.name = data?.name;
        return record;
      });

      Swal.fire({
        icon: 'success',
        title: 'Successfully created',
      });

      toggleMode();
    }
  };
  const isInputValid = (input, validations) => {
    let isValid = true;

    if (validations.required) {
      isValid = isValid && input.length !== 0;
    }
    if (validations.minLength) {
      isValid = isValid && input.length >= validations.minLength;
    }
    if (validations.maxLength) {
      isValid = isValid && input.length <= validations.maxLength;
    }

    return isValid;
  };


  const handleInputChange = event => {
    const { name, value } = event.target;

    const updatedElement = { ...form[name] };
    updatedElement.value = value;
    updatedElement.touched = true;
    updatedElement.valid = isInputValid(value, updatedElement.validations);

    const updatedForm = { ...form, [name]: updatedElement }
    setForm(updatedForm);

    const formValid = Object.values(updatedForm).every(elem => elem.valid);
    setIsFormValid(formValid);
  }

  return (
    <>
      <div className="justify-content-center mb-3 mt-3">
        <div className="col-12">
          <form onSubmit={handleSubmit}>

            <div className="form-group">
              <input name="name" type="text" className="form-control"
                placeholder={form.name.placeholder}
                value={form.name.value}
                onChange={handleInputChange}
              />
            </div>

            <div className="form-group">
              <input name="startDate" type="date" className="form-control"
                placeholder={form.startDate.placeholder}
                value={form.startDate.value}
                onChange={handleInputChange}
              />
            </div>

            <div className="form-group">
              <input name="endDate" type="date" className="form-control"
                placeholder={form.endDate.placeholder}
                value={form.endDate.value}
                onChange={handleInputChange}
              >
              </input>
            </div>

            <div className="text-center">

              <button
                type="submit"
                className="btn btn-primary btn-lg mr-1 mb-2"
                disabled={!isFormValid}>
                Create
              </button>
              <button
                type="button"
                className="btn btn-primary btn-lg ml-1 mb-2"
                onClick={() => toggleMode()}
              >
                Cancel
            </button>
            </div>

          </form>
        </div>
      </div>

    </>
  )
}


export default CreateRecord;